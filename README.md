<h1 align="center">Electron Vue Antd Admin</h1>

<div align="center">
  
[Ant Design Pro](https://github.com/ant-design/ant-design-pro) 的 Electron-Vue 实现版本  
开箱即用的 桌面端 中后台前端/设计解决方案
</div>
这是一个基于electron+vue-antd-admin的跨平台桌面应用模板，整合了iczer / vue-antd-admin，修复了一些框架兼容问题，electron基础部分参考逍遥僧 / electron-vue-ant项目，感谢 iczer 逍遥僧 二位大佬的开源项目。

<div align="center">

预览：

![image](./src/assets/img/preview.png)

多种主题模式可选：

![image](./src/assets/img/preview-nine.png)
</div>


## 感谢
iczer / vue-antd-admin项目地址，推荐大家学习
https://gitee.com/iczer/vue-antd-admin

逍遥僧 / electron-vue-ant项目地址，推荐大家学习
https://gitee.com/xiaoyaoseng/electron-vue-ant


## 使用
### clone
```bash
$ git clone https://gitee.com/gqf1995/electron-vue-antd-admin.git
```
### cnpm
```
$ cnpm install
$ cnpm run dev
```
### 打包
```
$ cnpm run build
```
vue-antd-admin更多信息参考 [使用文档](https://iczer.gitee.io/vue-antd-admin-docs)

### 常见报错
```
fsevents是mac系统的一个必须要有的二进制模块，如果你想开发跨平台程序，一定绕不开他。

这个报错的原因是fsevents.js中使用了require，导致项目无法启动。

需要点击./node_modules/_fsevents@2.3.2@fsevents/fsevents.js 13:15，找到该文件13行代码，按照下文修改即可

// ./node_modules/_fsevents@2.3.2@fsevents/fsevents.js

const Native = require("./fsevents.node")

//更改为

const Native = window.require("./fsevents.node")
```

