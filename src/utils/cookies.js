

const { session } = require('electron')
const Cookie = {}

Cookie.get = (name) => {
    return new Promise(((resolve, reject) => {
        // 查询所有与设置的 URL 相关的所有 cookies.
        session.defaultSession.cookies.get({ url: 'http://www.github.com', name })
            .then((cookies) => {
                resolve(cookies)
            }).catch((error) => {
                reject(error)
            })
    }))
}


// 设置一个 cookie，使用设置的名称；
// 如果存在，则会覆盖原先 cookie.
Cookie.set = (name, value) => {
    return new Promise((resolve, reject) => {
        const cookie = { url: 'http://www.github.com', name, value }
        session.defaultSession.cookies.set(cookie)
            .then(() => {
                resolve()
            }, (error) => {
                reject(error)
            })
    })
}
Cookie.remove = (name) => {
    return new Promise((resolve, reject) => {
        const cookie = { url: 'http://www.github.com', name, value: null }
        session.defaultSession.cookies.set(cookie)
            .then(() => {
                resolve()
            }, (error) => {
                reject(error)
            })
    })
}
module.exports = Cookie 