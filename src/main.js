import Vue from 'vue'
import App from './App.vue'
import { initRouter } from './router'
//import '../public/color.less'
import './theme/index.less'
import Antd from 'ant-design-vue'
//antV图表引入会build打包报错 用echarts
// import Viser from 'viser-vue'
import '@/mock'
import store from './store'
import 'animate.css/source/animate.css'
import Plugins from '@/plugins'
import bootstrap from '@/bootstrap'
import 'moment/locale/zh-cn'

const router = initRouter(store.state.setting.asyncRoutes)
import { initI18n } from '@/utils/i18n'
const i18n = initI18n('CN', 'US')
import { Ohyeah } from "vue-ohyeah-scroll";
Vue.use(Ohyeah);
Vue.use(Antd)
Vue.config.productionTip = false
// Vue.use(Viser)
Vue.use(Plugins)

bootstrap({ router, store, i18n, message: Vue.prototype.$message })

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
}).$mount('#app')
