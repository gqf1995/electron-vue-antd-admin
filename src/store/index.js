import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
// import { createPersistedState, createSharedMutations } from "vuex-electron"
// import { createPersistedState } from "vuex-electron"

Vue.use(Vuex)
const store = new Vuex.Store({modules})

export default store

// export default new Vuex.Store({
//     modules,
//     plugins: [
//         createPersistedState(),
//         createSharedMutations()
//     ],
// })